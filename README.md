# SNAP Sentinel-2 demo: Satellite data manipulation using SNAP within Jupyter Notebook
## Sentinel-2 unsupervised classification exercise
Run the application from Binder clicking to the logo here:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mdelgadoblasco%2Fnotebooks_snap_demo_s2/HEAD?urlpath=lab)

This has been created by Earth Observation Platform & Phi-Lab Engineering Support team (PLES team).
Any enquiry send an email to : PLES_management@rheagroup.com

This repository is licensed with : **[European Space Agency Community License – v2.4 Permissive](https://essr.esa.int/license/european-space-agency-community-license-v2-4-permissive)**

There are several notebooks which:
- main notebook: S2_classification which classifies Normalised indices derived from Sentinel-2 data using SNAP
- 1 more notebook included in resources folder:
    - Results_visualisation plots the results over a basemap for visual inspection.


The notebooks can be modified to compute other indices or using other bands as well as customizing EM cluster parameters.

Launch the application from the logo above and enjoy!

